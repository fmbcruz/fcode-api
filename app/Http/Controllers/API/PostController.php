<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Post;
use App\Option;
use Validator;
use App\Http\Resources\Post as PostResource;
   
class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type') ?: 'post';

        $post = Post::with([
            'user' =>function($query){$query->select('id','name');},
            'parents',
        ])->where('type', '=', $type)->get();
    
        return $this->sendResponse(PostResource::collection($post), 'Data retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'user_id' => 'required',
            'type' => 'required',
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post = Post::create($input);

        return $this->sendResponse(new PostResource($post), 'Data created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with([
            'user' =>function($query){$query->select('id','name');},
            'parents',
        ])->where('id', $id)->get();
  
        if (is_null($post)) {
            return $this->sendError('Data not found.');
        }

        return $this->sendResponse(new PostResource($post), 'Data retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $input = $request->all();
        $post = Post::find($id);
        
        $validator = Validator::make($input, [
            'user_id' => 'required',
            'type' => 'required',
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post->user_id = $request->get('user_id');
        $post->type = $request->get('type');
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->excerpt = $request->get('excerpt');
        $post->slug = $request->get('slug');
        $post->slug = $request->get('parent_id');
        $post->save();

        return $this->sendResponse(new PostResource($post), 'Data updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
   
        return $this->sendResponse([], 'Data deleted successfully.');
    }
}