# API Filipe Cruz

[![N|Laravel](https://laravel.com/img/logotype.min.svg)](https://laravel.com/)

API for management personal website

### Installation

Configure .env with enviroment variables.

Install the dependencies and devDependencies and start the server.

```sh
$ composer install
$ php artisan migrate
$ php artisan passport:install
$ php artisan key:generate 
$ php artisan serve
$ php artisan config:cache
```

### Login

For to login, access the route http://localhost:8000/api/login with email and password.